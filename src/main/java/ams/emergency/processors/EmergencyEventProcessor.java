package ams.emergency.processors;

import ams.data.model.EmergencyEventModel;
import ams.emergency.jpa.EmergencyAccident;
import ams.emergency.jpa.EmergencyAccidentRepository;
import ams.emergency.mapper.EmergencyMapper;
import ams.emergency.service.EmergencyAccidentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class EmergencyEventProcessor {
    private final EmergencyAccidentService emergencyAccidentService;

    public void process(List<ConsumerRecord<String, EmergencyEventModel>> records) {
        log.info("total messages - {}", records.size());

        List<EmergencyAccident> batch = new ArrayList<>();
        try {
            for (ConsumerRecord<String, EmergencyEventModel> rec : records){
                log.info("Record: value - {}, key - {}", rec.value(), rec.key());
                //collect in a batch
                batch.add(EmergencyMapper.MAPPER.emergencyEventModelToEmergencyAccident(rec.value()));
            }
        } catch (Exception e) {
            log.error("Error processing record", e);
            throw e;//re-throw an exception to trigger the recoverer
        } finally {
            log.info("batch size - {}", batch.size());
            emergencyAccidentService.saveBatch(batch);//in case an exception still save a batch we got before the exception
            log.info("batch saved");
        }
    }

}
