package ams.emergency.exception;

public class InvalidEventException extends RuntimeException {
    public InvalidEventException() {
        super("Invalid event exception");
    }
}
