package ams.emergency.service;

import ams.emergency.jpa.EmergencyAccident;
import ams.emergency.jpa.EmergencyAccidentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmergencyAccidentService {
    private final EmergencyAccidentRepository emergencyAccidentRepository;

    @Transactional(rollbackFor = Exception.class)
    public void saveBatch(List<EmergencyAccident> batch){
        emergencyAccidentRepository.saveAll(batch);
    }
}
