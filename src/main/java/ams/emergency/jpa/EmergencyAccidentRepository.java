package ams.emergency.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmergencyAccidentRepository extends CrudRepository<EmergencyAccident, Long> {
}
