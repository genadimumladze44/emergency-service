# Accident Management Service(AMS)

## Project Description
Accident Management Service(AMS) is the project implemented with Event-Driven Architectural approach </br>
that provides messages distribution across several services based on their event type, `ACCIDENT_TYPE`. </br>
It is simulation of what happens when some kind of accident happens, </br>
it can be criminal, car, fire or some kind of accident that are saved in some </br>
(in case of default settings: `accident.events`) source topic.</br>

### Visual
![alt text](./materials/AMS-Project-Architecture.png)

## Emergency Service application description
Emergency Service application reads and processes the incoming emergency events from specific sink topics</br>
(in case of default settings: `emergency.events`) produced by event stream application. If some error arises</br>
the application sends it to dead letter topic(in case of default settings: `emergency.events-dlt`) and continues process</br>
for other ones. Since data processed successfully it will be stored in postgres SQL database.

## Getting started
### Installation requirements
In order to start up the application you need have installed:
- Docker on you machine </br>
- git </br>
  or
- Apache kafka cluster with at least three brokers
- git
- maven 3.9.x
- JDK-17

### Installation steps
The project installation could be done using docker-compose.yml via command line interface (CMD):
```
git clone https://gitlab.com/genadigeno/emergency-service.git && 
cd emergency-service && 
docker compose up
```
or
```
git clone https://gitlab.com/genadigeno/emergency-service.git && 
cd emergency-service && 
mvn clean package
```
```
java -DPOSTGRES_URL=jdbc:postgresql://localhost:5432/postgres -DPOSTGRES_USER=postgres -DPOSTGRES_PASSWORD= -jar ./target/emergency-service.jar
```
### JVM Parameters
- `SERVER_PORT` - application port number, default: 8080
- `KAFKA_MAIN_TOPIC_NAME` - kafka topic name for emergency service, default: emergency.events
- `KAFKA_DLT_TOPIC_NAME` - kafka dead letter topic name for emergency service application, default: emergency.events-dlt
- `KAFKA_BOOTSTRAP_SERVERS` - kafka cluster url, default: localhost:9092,localhost:9093
- `SCHEMA_REGISTRY_HOST` - schema registry url, default: http://localhost:8081  
- `POSTGRES_URL` - postgres url, default: jdbc:postgresql://localhost:5432/postgres
- `POSTGRES_USER` - postgres user, default: postgres
- `POSTGRES_PASSWORD` - postgres password

***
### Used Technologies
- <img src="./materials/eda.png" height="15" alt="img"> Event-Driven Architecture
- <img src="./materials/java.png" height="15" alt="img"> Java 17
- <img src="./materials/boot.png" height="15" alt="img"> Spring Boot & Spring Boot JPA
- <img src="./materials/kafka.png" height="15" alt="img"> Apache Kafka & Kafka Streams library
- <img src="./materials/docker.png" height="15" alt="img"> Docker
- <img src="./materials/git.png" height="15" alt="img"> Git
- <img src="./materials/gitlab-ci-cd.png" height="15" alt="img"> Gitlab CI/CD
### Project status
Completed
